# -*- coding: utf-8 -*-
# Generated by Django 1.9.6 on 2016-05-03 20:51
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Mesa',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('num_mesa', models.IntegerField()),
                ('qtd_cadeiras', models.IntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='Reserva',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('data', models.DateField(verbose_name='data_reserva')),
                ('horario', models.CharField(max_length=5)),
                ('nomeCliente', models.CharField(max_length=100)),
                ('emailCliente', models.CharField(max_length=50)),
                ('telefoneCliente', models.CharField(max_length=12)),
                ('mesa', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='polls.Mesa')),
            ],
        ),
        migrations.CreateModel(
            name='Restaurante',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nome', models.CharField(max_length=50)),
                ('descricao', models.CharField(max_length=50)),
                ('url_foto', models.CharField(max_length=200)),
                ('endereco', models.CharField(max_length=100)),
                ('telefone', models.CharField(max_length=12)),
            ],
        ),
        migrations.RemoveField(
            model_name='choice',
            name='question',
        ),
        migrations.DeleteModel(
            name='Choice',
        ),
        migrations.DeleteModel(
            name='Question',
        ),
        migrations.AddField(
            model_name='reserva',
            name='restaurante',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='polls.Restaurante'),
        ),
        migrations.AddField(
            model_name='mesa',
            name='restaurante',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='polls.Restaurante'),
        ),
    ]
