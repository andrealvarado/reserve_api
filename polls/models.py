import datetime

from django.db import models
from django.utils import timezone

class Restaurante(models.Model):
    nome = models.CharField(max_length=50)
    descricao = models.CharField(max_length=50)
    url_foto = models.CharField(max_length=200)
    endereco = models.CharField(max_length=100)
    telefone = models.CharField(max_length=12)
    
    def __str__(self):
        return self.nome
        
        
    def as_json(self):
        return dict(
            id=self.id,
            nome=self.nome,
            descricao=self.descricao,
            url_foto=self.url_foto,
            endereco=self.endereco,
            telefone=self.telefone
        )
    
class Mesa(models.Model):
    restaurante = models.ForeignKey(Restaurante, on_delete=models.CASCADE)
    num_mesa = models.IntegerField()
    qtd_cadeiras = models.IntegerField()
    
    def __str__(self):
        return self.num_mesa
    
class Reserva(models.Model):
    restaurante = models.ForeignKey(Restaurante, on_delete=models.CASCADE)
    mesa = models.ForeignKey(Mesa, on_delete=models.CASCADE)
    data = models.DateField('data_reserva')
    horario = models.CharField(max_length=5)
    nomeCliente = models.CharField(max_length=100)
    emailCliente = models.EmailField()
    telefoneCliente = models.CharField(max_length=12)
    
    def __str__(self):
        return self.data
               