from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^restaurantes/$', views.restaurantes, name='restaurantes'),
    url(r'^horarios/$', views.horarios, name='horarios')

]