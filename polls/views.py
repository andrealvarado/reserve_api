from django.http import HttpResponse
from polls.models import Restaurante
import json
from collections import namedtuple
from django.db import connection
from django.db.models import Count
from django.views.decorators.csrf import csrf_exempt
from django.http import JsonResponse
from django.http.request import QueryDict


def namedtuplefetchall(cursor):
    desc = cursor.description
    nt_result = namedtuple('Result', [col[0] for col in desc])
    return [nt_result(*row) for row in cursor.fetchall()]
    
def restaurantes(request):
    restaurantes = Restaurante.objects.all()
    results = [ob.as_json() for ob in restaurantes]
    return HttpResponse(json.dumps(results), content_type="application/json")

@csrf_exempt      
def horarios(request):
    restaurante_id = request.POST.get("id")
    data = request.POST.get("data")
       
    cursor = connection.cursor()
    cursor.execute("SELECT restaurante_id, COUNT(num_mesa) FROM polls_mesa WHERE restaurante_id = %s GROUP BY restaurante_id ORDER BY restaurante_id", [restaurante_id]);
    results = namedtuplefetchall(cursor) 
        
    cursor.execute(" SELECT horario, COUNT(horario) as qtd_mesas_reservadas, restaurante_id FROM polls_reserva WHERE restaurante_id = %s and data = %s GROUP BY horario, restaurante_id ORDER BY horario", [restaurante_id, data]);
    results2 = namedtuplefetchall(cursor)
    lista = list()    
    for result in results2:
        if result.qtd_mesas_reservadas < results[0].count: 
            lista.append(result.horario)
                
    d = {"data": lista}
    return JsonResponse(d)
   
  